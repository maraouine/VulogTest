# VulogTest

This project is for Vulog technical test.
Target : test https://top-movies-qhyuvdwmzt.now.sh/ website. 

Perimeter : tests requested
1. Open the application and make sure a list of movie tiles is displayed.
2. Open the movie The Shawshank Redemption and make sure the release date is
correctly displayed.
3. Retrieve the url of the image and open it in another tab then close the tab.
4. Search for Star Trek and make sure that the movie Star Trek: First Contact is displayed in
the search results and the movie The Shawshank Redemption is no longer visible.
5. Take any movie you like and make sure the Released on, popularity, vote average
and vote count fields have the expected values.

## How to start test locally ?

The version used during development are :

Java : java version "11.0.15"
Mvn : Apache Maven 3.8.2


#  Prerequisite #
-maven : https://phoenixnap.com/kb/install-maven-windows 
-java : https://www.guru99.com/install-java.html

Please don't forget to verify if maven and java are well installed by doing "mvn -version" and "java -version" 

#  Step 1 : clone repository #
Clone repository in your local storage by doing :

	git clone https://github.com/maraouine/VulogTest.git

#  Step 2 : Start test #

Go under workspace directory and start this command : 

	mvn test 

#  Step 3 : Consult results #

After the launch of the tests a folder named target is created. Inside the report in different format.

Html Report
![htmlReport1](https://user-images.githubusercontent.com/61714215/187939376-217f1e0f-b754-4583-8852-126e4903bfc7.PNG)
![htmlReport2](https://user-images.githubusercontent.com/61714215/187939406-86966f84-965d-4fdb-8a76-66cf70bb85ae.PNG)


## Contributing

Contact Mohamed Amine Raouine by email: raouine.mohamed20@gmail.com , if you need any help 


## Evolution to do 
 - Add logger 
 - Handle environnement by adding the environnement as parameter (QA, DEV, prod, preprod)
 - Create a CI/CD 
