package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.BaseTest;


public class TopMoviesPage extends BaseTest {
	

		
	    By PageTitle= By.xpath("//h2[@class='jss39 jss45 jss58 title']"); 
	    By MoviesTiles=By.xpath("//div[@class='jss10 jss14 jss11 movie-card']");
	    By MoviesTitle=By.xpath("//h2[@class='jss39 jss44']");
	    By learnMore=By.xpath("//button[@class='jss109 jss94 jss96 jss106 jss93']");
	    By searchElement=By.xpath("//input[@type='search']");
	    
	    public String getPageTitle() {	    
	       return  driver.findElement(PageTitle).getText();
	    }
	
	    public void clickOnLearnMore() throws InterruptedException
	    {
	    	clickOn(learnMore);
	    }
	    public Boolean isMoviesTilesDisplayed() {
	    	Boolean isMoviesTilesDisplayed=false;
	    	isMoviesTilesDisplayed=isElementDisplayedOnPage(MoviesTiles);
	    	return isMoviesTilesDisplayed;
		}
	    
	    public void SelectMovieFromMoviesList(String movieName){
	    	SelectMovieFromList(MoviesTitle, learnMore, movieName);
	    }
	      
	    public void SearchMovie(String movieName)
	    {
	    	sendKeys(searchElement, movieName);
	    }
	    
	    public Boolean isMovieSearchedDisplayed(String MovieSearched)
	    {
	    	return isMovieDisplayedFromList(MoviesTitle,MovieSearched);
	    }

	    public Boolean isMovieDisplayedFromListContainsPhrase(String MovieSearched)
	    {
	    	return isMovieDisplayedFromListContainsPhrase(MoviesTitle,MovieSearched);
	    }
	    
	
}
