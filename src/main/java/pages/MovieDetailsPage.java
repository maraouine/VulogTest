package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import base.BaseTest;

public class MovieDetailsPage extends BaseTest{

        
    By MoviesDetailsFileds=By.xpath("//input[@class='jss71 jss73']");
    By ReleaseDateField=By.xpath("(//input[@class='jss71 jss73'])[1]");
    By PopularityField=By.xpath("(//input[@class='jss71 jss73'])[2]");
    By VoteAverageField=By.xpath("(//input[@class='jss71 jss73'])[3]");
    By VoteCountField=By.xpath("(//input[@class='jss71 jss73'])[4]");
    By image=By.xpath("//div[@class='jss89 movie-image']");
    
    public void ScrollToMoviesDetailsWindow() {
     JavascriptExecutor js = (JavascriptExecutor) driver;     
     js.executeScript("arguments[0].scrollIntoView();", driver.findElement(MoviesDetailsFileds));
    }
    
	
    public String getReleaseDate() {
    	String releaseDate="";
    	releaseDate=getElementValue(ReleaseDateField);
    	return releaseDate;
	    }
    public String getPopularity() {
    	String getPopularity="";
    	getPopularity=getElementValue(PopularityField);
    	return getPopularity;
	    }
    public String getVoteAverage() {
    	String getVoteAverage="";
    	getVoteAverage=getElementValue(VoteAverageField);
    	return getVoteAverage;
	    }
    public String getVoteCount() {
    	String getVoteCount="";
    	getVoteCount=getElementValue(VoteCountField);
    	return getVoteCount;
	    }
    public String getTheImageURL()
    {
    	String URL="";
    	URL=getCSSValue(image, "background-image");
    	URL=URL.split("url[(]\"")[1].split("\"[)]")[0];
    	return URL;
    }
}

