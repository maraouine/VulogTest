package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.messages.types.Duration;
import io.github.bonigarcia.wdm.WebDriverManager;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.messages.types.Duration;

public class BaseTest {

    public static Properties properties= new Properties();
	public static WebDriver driver=null;
	public static String browser=null;
	public static String url=null;

	public WebDriver getDriver() {
		return this.getDriver();
	}
	public void setDriver(WebDriver driver){
		this.driver=driver;
	}
	
    public static void closeDriver(){
        driver.close();
    }
    
    public String readPropertiesFile(String param)
    {
    	try {
			InputStream inputStream=new FileInputStream(System.getProperty("user.dir")+"/src/main/resources/config/config.properties");
			properties.load(inputStream);

		} catch (Exception e) {
			e.printStackTrace();
		}
    	
       return properties.getProperty(param);

    }
    public void init()
    {
        browser =readPropertiesFile("browser");
        url = readPropertiesFile("url");
        if (browser == null) {
            browser = "chrome";
        }
        switch (browser) {
	        case "chrome":
				WebDriverManager.chromedriver().setup();
	            ChromeOptions options=new ChromeOptions();
	            options.setHeadless(true);
	            options.addArguments("window-size=1920,1080");
	            driver=new ChromeDriver(options);
	            driver.manage().window().maximize();
	        break;
        }
        driver.navigate().to(url);
    }
    
    /*
     * PUT the 10 number on config file
     */
    public void waitForVisibility(By element) {
    	WebElement wait = new WebDriverWait(driver, java.time.Duration.ofSeconds(10))
    	        .until(ExpectedConditions.presenceOfElementLocated(element));
	}
    public void sendKeys(By element, String txt) {
    	  waitForVisibility(element);
    	  driver.findElement(element).sendKeys(txt);
    	  driver.findElement(element).sendKeys(Keys.ENTER);
      } 
    public void clickOn(By element){
  	  waitForVisibility(element);
  	  driver.findElement(element).click();;
    } 
    
    public String getElementValue(By element) {
        return driver.findElement(element).getAttribute("value");
	    }
    public String getCSSValue(By element, String proprertyName) {
        return driver.findElement(element).getCssValue(proprertyName);
	    }
    public Boolean isElementDisplayedOnPage(By element)
    {
    	Boolean isElementDisplayedOnPage=false;
    	waitForVisibility(element);
    	if(driver.findElement(element).isDisplayed())
    	{
    		isElementDisplayedOnPage=true;
    	}
    	return isElementDisplayedOnPage;
    }
    
    public void SelectMovieFromList(By movietitleElement, By learnMoreBTNElement, String movieSearched)
    {
    	   List<WebElement> moviesTitlesList = driver.findElements(movietitleElement);
    	   WebElement learnMoreBTN=driver.findElement(learnMoreBTNElement);

    	   for(int i = 0; i< moviesTitlesList.size(); i++) {
    	    	 
    		     System.out.println("Movie name: " + moviesTitlesList.get(i).getText());
  	             if(moviesTitlesList.get(i).getText().equalsIgnoreCase(movieSearched))
    	    	 {
    	    		 learnMoreBTN.click();
    	    		 break;
    	    	 }
    	   
    	      }
    }
	
    public Boolean isMovieDisplayedFromList(By movieTitleElement, String movieSearched)
    {
    	Boolean isMovieDisplayedFromList=false;
 	    List<WebElement> moviesTitlesList = driver.findElements(movieTitleElement);
 	    
 	    for(int i = 0; i< moviesTitlesList.size(); i++) {
 	    	
	     System.out.println("Movie name: " + moviesTitlesList.get(i).getText());
 	     if(moviesTitlesList.get(i).getText().equalsIgnoreCase(movieSearched))
    	 {
 	   		isMovieDisplayedFromList=true;
    		 break;
    	 }
 	    }
    	return isMovieDisplayedFromList;
    }
    
    public Boolean isMovieDisplayedFromListContainsPhrase(By movieTitleElement, String pharseSearched)
    {
    	Boolean isMovieDisplayedFromListContainsPhrase=false;
 	    List<WebElement> moviesTitlesList = driver.findElements(movieTitleElement);
 	    
 	    for(int i = 0; i< moviesTitlesList.size(); i++) {
 	    	
	    System.out.println("Movie name: " + moviesTitlesList.get(i).getText());
 	    if(moviesTitlesList.get(i).getText().contains(pharseSearched))
    	{
 	    	isMovieDisplayedFromListContainsPhrase=true;
    	}
 	    else 
 	    {
 	    	isMovieDisplayedFromListContainsPhrase=false;
 		    System.out.println("Movie dosen't contain phrase: " + moviesTitlesList.get(i).getText());
 	    	break;
 	    }
 	    }
    	return isMovieDisplayedFromListContainsPhrase;
    }

}
