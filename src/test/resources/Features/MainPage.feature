Feature: Main page
  Scenario: 1-Verify the display of list of movies tiles
    Given I am on the Top Movies Page
    Then I can see the list of movie tiles
  Scenario: 2-Verify if the release date is displayed
    Given I am on the Top Movies Page
    When I select the movies "The Shawshank Redemption"
    Then I can see the date "1994-09-23" as release movie date  
  Scenario: 3- Retrieve the url of the image and open it in another tab then close the tab
    Given I am on the Top Movies Page
    When I search the movie "The Croods: A New Age"
    And I Retrieve the url of the image
 		And I Open the image in another tab
 		And I close the tab
  Scenario: 4- Movie Search
    Given I am on the Top Movies Page
    When I search the movie "Star Trek"
    Then I can see the movie "Star Trek: First Contact" from movies list
    And I am not able to find the movie "The Shawshank Redemption" anymore 
  Scenario: 5- Movie Search - movie search phrase
    Given I am on the Top Movies Page
    When I search the movie "A New"
    Then I can see the movie whose title contains "New" search phrase
  Scenario Outline: 6- Verify values of Released on, popularity, vote average and vote count fields
    Given I am on the Top Movies Page
    When I search the movie "The Croods: A New Age"
    And I click on learn more
    Then I verify if the release on value is : "<Released on>"
    And I verify if the popularity value is : "<popularity>"
    And I verify if the vote average value is : "<vote average>"
    And I verify if the vote count value is : "<vote count>"    
   Examples:
     | Released on  | popularity  | vote average | vote count |
     | 2020-11-25   | 195.897    |   7.6	       | 3090       |

