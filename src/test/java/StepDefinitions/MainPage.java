package StepDefinitions;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.WindowType;

import base.BaseTest;
import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.MovieDetailsPage;
import pages.TopMoviesPage;

public class MainPage extends BaseTest {
	
	TopMoviesPage topMoviesPage=new TopMoviesPage();
	MovieDetailsPage movieDetailsPage=new MovieDetailsPage();
	public String urlImage;
	@Before 
	public void setup ()
	{
		init();
	}
	@Given("I am on the Top Movies Page")
	public void i_am_on_the_top_movies_page() {
		Assert.assertTrue(topMoviesPage.getPageTitle().equalsIgnoreCase("Top Movies"));
	}

	@Then("I can see the list of movie tiles")
	public void i_can_see_the_list_of_movie_tiles() {
		Assert.assertTrue(topMoviesPage.isMoviesTilesDisplayed());
	}
	
	@When("I select the movies {string}")
	public void i_select_the_movies(String string) {
		topMoviesPage.SelectMovieFromMoviesList(string);
	}

	
	@Then("I can see the date {string} as release movie date")
	public void i_can_see_the_date_as_release_movie_date(String expectedMovieReleaseDate) {
		movieDetailsPage.ScrollToMoviesDetailsWindow();
		Assert.assertTrue(movieDetailsPage.getReleaseDate().equalsIgnoreCase(expectedMovieReleaseDate));
	}
	@When("I search the movie {string}")
	public void i_search_the_movie(String movieName) throws InterruptedException {
		topMoviesPage.SearchMovie(movieName);
	}

	@Then("I can see the movie {string} from movies list")
	public void i_can_see_the_movies_from_movies_list(String movieSearched) throws InterruptedException {
		Thread.sleep(3000);
		Assert.assertTrue(topMoviesPage.isMovieSearchedDisplayed(movieSearched));
	}

	@Then("I am not able to find the movie {string} anymore")
	public void i_am_not_able_to_find_the_movies_anymore(String movieSearched) {
		Assert.assertFalse(topMoviesPage.isMovieSearchedDisplayed(movieSearched));
	}

	@Then("I can see the movie whose title contains {string} search phrase")
	public void i_can_see_the_movies_whose_title_contains_search_phrase(String moviePharse) throws InterruptedException {
		Thread.sleep(3000);
		Assert.assertTrue(topMoviesPage.isMovieDisplayedFromListContainsPhrase(moviePharse));
	}
	
	@When("I click on learn more")
	public void i_click_on_learn_more() throws InterruptedException {
		Thread.sleep(2000);
		topMoviesPage.clickOnLearnMore();

		Thread.sleep(3000);

	}
	@Then("I can see the {string}, {string}, {string} and {string} fields with the expected values")
	public void i_can_see_the_and_fields_with_the_expected_values(String releaseOn, String popularity, String voteaverage, String votecount) {
		Assert.assertTrue(movieDetailsPage.getReleaseDate().equalsIgnoreCase(releaseOn));
		Assert.assertTrue(movieDetailsPage.getPopularity().equalsIgnoreCase(popularity));
		Assert.assertTrue(movieDetailsPage.getVoteAverage().equalsIgnoreCase(voteaverage));
		Assert.assertTrue(movieDetailsPage.getVoteCount().equalsIgnoreCase(votecount));

	}
	
	@Then("I verify if the release on value is : {string}")
	public void i_verify_if_the_release_on_value_is(String releaseOnDate) {
		Assert.assertTrue(movieDetailsPage.getReleaseDate().equalsIgnoreCase(releaseOnDate));
	}

	@Then("I verify if the popularity value is : {string}")
	public void i_verify_if_the_popularity_value_is(String popularity) {
		Assert.assertTrue(movieDetailsPage.getPopularity().equalsIgnoreCase(popularity));
	}

	@Then("I verify if the vote average value is : {string}")
	public void i_verify_if_the_vote_average_value_is(String voteaverage) {
		Assert.assertTrue(movieDetailsPage.getVoteAverage().equalsIgnoreCase(voteaverage));
	}

	@Then("I verify if the vote count value is : {string}")
	public void i_verify_if_the_vote_count_value_is(String votecount) {
		Assert.assertTrue(movieDetailsPage.getVoteCount().equalsIgnoreCase(votecount));
	}
	
	@When("I Retrieve the url of the image")
	public void i_retrieve_the_url_of_the_image() throws InterruptedException {
		Thread.sleep(3000);
		urlImage=movieDetailsPage.getTheImageURL();

	}

	@When("I Open the image in another tab")
	public void i_open_the_image_in_another_tab() throws InterruptedException {
		driver.switchTo().newWindow(WindowType.TAB);
		driver.get(urlImage);
		Thread.sleep(3000);

	}

	@When("I close the tab")
	public void i_close_the_tab() throws InterruptedException {
		
		Set<String> handles=driver.getWindowHandles();
		List<String> ls= new ArrayList<String>(handles);
		
		String parentWindowID=ls.get(0);
		String childWindowID=ls.get(1);
		driver.close();
		//wait to see if the tab is closed
		Thread.sleep(5000);

		driver.switchTo().window(parentWindowID);

	}
	
	
	@After
    public void teardown() {
        driver.quit();
    }


}
